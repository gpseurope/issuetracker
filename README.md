# issuetracker

This project contains the issue tracker for the gpsgroup within the EPOS project. 
All issues are processed, matured and validated on this platform.
All issues from both GNSS group developers and end-users are located here.

Please describe the issue as much a spossible with examples where apropriate.

## Getting started

Please use labels to identify a specific project.

There is a specific "general" label for those bug/error isuees that are difficult to attribute to a specific project.
If you know the specific project and there is no label use the "general" and specify the project within the description

List of Project Names : (Extra Information in Brackets)

Data Retrieval Projects

- GlassWebClient               (Glass Node GUI : http://gnssdata-epos.oca.eu/ )

- GlassFramework API           (For Station, Rinex file and Quality API   http://gnssdata-epos.oca.eu:8080/GlassFramework )
- GlassFramework API PRODUCTS  (For the Coordinates, TimeSeries, Velocities API	https://glass.epos.ubi.pt:8080/GlassFramework/ )

- Pyglass                      (Command Line Download Script )

- Products Portal              (The product portal GUI : https://gnssproducts.epos.ubi.pt/ )	

Data Insertion Projects

- fwss	 (Flask Web Server and Application)		
- RunQC  (Quality Control)			 
- Indexgd (rinex file metadata extraction)

Some of the Other Main Projects			
- Sync. Service		
- Test and Data		 	
